# echo "cleanup InDetPerformanceMonitoring InDetPerformanceMonitoring-00-03-16 in /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtInDetPerformanceMonitoringtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtInDetPerformanceMonitoringtempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetPerformanceMonitoring -version=InDetPerformanceMonitoring-00-03-16 -path=/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring  $* >${cmtInDetPerformanceMonitoringtempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=InDetPerformanceMonitoring -version=InDetPerformanceMonitoring-00-03-16 -path=/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring  $* >${cmtInDetPerformanceMonitoringtempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetPerformanceMonitoringtempfile}
  unset cmtInDetPerformanceMonitoringtempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtInDetPerformanceMonitoringtempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtInDetPerformanceMonitoringtempfile}
unset cmtInDetPerformanceMonitoringtempfile
return $cmtcleanupstatus

