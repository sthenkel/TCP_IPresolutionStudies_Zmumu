# echo "setup InDetPerformanceMonitoring InDetPerformanceMonitoring-00-03-16 in /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.7.3/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetPerformanceMonitoringtempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetPerformanceMonitoringtempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetPerformanceMonitoring -version=InDetPerformanceMonitoring-00-03-16 -path=/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring  -no_cleanup $* >${cmtInDetPerformanceMonitoringtempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=InDetPerformanceMonitoring -version=InDetPerformanceMonitoring-00-03-16 -path=/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring  -no_cleanup $* >${cmtInDetPerformanceMonitoringtempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtInDetPerformanceMonitoringtempfile}
  unset cmtInDetPerformanceMonitoringtempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtInDetPerformanceMonitoringtempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtInDetPerformanceMonitoringtempfile}
unset cmtInDetPerformanceMonitoringtempfile
exit $cmtsetupstatus

