#Mon May 16 18:30:50 2016"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *

class IDPerfMonEoverP( ConfigurableAlgorithm ) :
  __slots__ = { 
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'ErrorCount' : 0, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'RefitTracks' : True, # bool
    'isDATA' : True, # bool
    'ReFitterTool' : PublicToolHandle(''), # GaudiHandle
    'ReFitterTool2' : PublicToolHandle(''), # GaudiHandle
    'TrigDecisionTool' : PublicToolHandle('Trig::TrigDecisionTool/TrigDecisionTool'), # GaudiHandle
    'InputElectronContainerName' : 'Electrons', # str
    'InputJetContainerName' : 'AntiKt4LCTopoJets', # str
    'METFinalName' : 'FinalClus', # str
    'MissingEtObjectName' : 'MET_Reference_AntiKt4LCTopo', # str
    'primaryVertexCollection' : 'PrimaryVertices', # str
    'RefittedElectronTrackContainer1' : 'GSFTracks', # str
    'RefittedElectronTrackContainer2' : 'DNATracks', # str
    'ValidationMode' : True, # bool
    'FillDetailedTree' : False, # bool
    'ElectronLikelihoodTune' : 'mc15_20150712', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'RegisterForContextService' : """ The flag to enforce the registration for Algorithm Context Service """,
    'ReFitterTool2' : """ ToolHandle for track fitter implementation """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'ReFitterTool' : """ ToolHandle for track fitter implementation """,
    'TrigDecisionTool' : """ The TrigDecisionTool instance. """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMonEoverP, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMonEoverP'
  pass # class IDPerfMonEoverP

class IDPerfMonKshort( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ProcessNEvents' : 0, # int
    'histoPathBase' : '', # str
    'PreScale' : 0, # int
    'TriggerChain' : '', # str
    'TriggerGroup' : '', # str
    'ManagerName' : 'AthenaMonManager', # str
    'TrigDecisionTool' : PublicToolHandle(''), # GaudiHandle
    'TriggerTranslatorTool' : PublicToolHandle(''), # GaudiHandle
    'FilterTools' : PublicToolHandleArray([]), # GaudiHandleArray
    'EnableLumi' : False, # bool
    'DetailLevel' : 1, # int
    'FileKey' : '', # str
    'DataType' : 'userDefined', # str
    'Environment' : 'noOutput', # str
    'tracksName' : '', # str
    'CheckRate' : 1000, # int
    'triggerChainName' : 'NoTriggerSelection', # str
    'VxContainerName' : 'V0UnconstrVertices', # str
    'VxPrimContainerName' : 'PrimaryVertices', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMonKshort, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMonKshort'
  pass # class IDPerfMonKshort

class IDPerfMonWenu( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ProcessNEvents' : 0, # int
    'histoPathBase' : '', # str
    'PreScale' : 0, # int
    'TriggerChain' : '', # str
    'TriggerGroup' : '', # str
    'ManagerName' : 'AthenaMonManager', # str
    'TrigDecisionTool' : PublicToolHandle(''), # GaudiHandle
    'TriggerTranslatorTool' : PublicToolHandle(''), # GaudiHandle
    'FilterTools' : PublicToolHandleArray([]), # GaudiHandleArray
    'EnableLumi' : False, # bool
    'DetailLevel' : 1, # int
    'FileKey' : '', # str
    'DataType' : 'userDefined', # str
    'Environment' : 'noOutput', # str
    'tracksName' : '', # str
    'electronsName' : 'Electrons', # str
    'photonsName' : 'Photons', # str
    'VxPrimContainerName' : 'PrimaryVertices', # str
    'emclustersName' : 'LArClusterEM', # str
    'metName' : 'MET_Reference_AntiKt4LCTopo', # str
    'METFinalName' : 'FinalClus', # str
    'eoverp_standard_min' : 0.50000000, # float
    'eoverp_standard_max' : 4.0000000, # float
    'eoverp_tight_min' : 0.70000000, # float
    'eoverp_tight_max' : 1.3000000, # float
    'CheckRate' : 1000, # int
    'triggerChainName' : 'NoTriggerSelection', # str
    'rejectSecondCluster' : True, # bool
    'electronIDLevel' : 'Tight', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMonWenu, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMonWenu'
  pass # class IDPerfMonWenu

class IDPerfMonZee( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ProcessNEvents' : 0, # int
    'histoPathBase' : '', # str
    'PreScale' : 0, # int
    'TriggerChain' : '', # str
    'TriggerGroup' : '', # str
    'ManagerName' : 'AthenaMonManager', # str
    'TrigDecisionTool' : PublicToolHandle(''), # GaudiHandle
    'TriggerTranslatorTool' : PublicToolHandle(''), # GaudiHandle
    'FilterTools' : PublicToolHandleArray([]), # GaudiHandleArray
    'EnableLumi' : False, # bool
    'DetailLevel' : 1, # int
    'FileKey' : '', # str
    'DataType' : 'userDefined', # str
    'Environment' : 'noOutput', # str
    'tracksName' : '', # str
    'electronsName' : 'Electrons', # str
    'photonsName' : 'Photons', # str
    'VxPrimContainerName' : 'PrimaryVertices', # str
    'emclustersName' : 'LArClusterEM', # str
    'metName' : 'MET_Reference_AntiKt4LCTopo', # str
    'METFinalName' : 'FinalClus', # str
    'eoverp_standard_min' : 0.50000000, # float
    'eoverp_standard_max' : 4.0000000, # float
    'eoverp_tight_min' : 0.70000000, # float
    'eoverp_tight_max' : 1.3000000, # float
    'CheckRate' : 1000, # int
    'triggerChainName' : 'NoTriggerSelection', # str
    'electronIDLevel' : 'Tight', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMonZee, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMonZee'
  pass # class IDPerfMonZee

class IDPerfMonZmumu( ConfigurableAlgorithm ) :
  __slots__ = { 
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'ErrorCount' : 0, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'triggerChainName' : '', # str
    'OutputTracksName' : 'ZmumuTracks', # str
    'doIsoSelection' : True, # bool
    'BeamCondSvc' : ServiceHandle('BeamCondSvc'), # GaudiHandle
    'Extrapolator' : PublicToolHandle('Trk::Extrapolator/AtlasExtrapolator'), # GaudiHandle
    'ReFitterTool1' : PublicToolHandle(''), # GaudiHandle
    'ReFitterTool2' : PublicToolHandle(''), # GaudiHandle
    'TrackToVertexTool' : PublicToolHandle('Reco::TrackToVertex'), # GaudiHandle
    'trackBiasingTool' : PublicToolHandle('InDet::InDetTrackBiasingTool'), # GaudiHandle
    'TrackToVertexIPEstimator' : PublicToolHandle('Trk::TrackToVertexIPEstimator'), # GaudiHandle
    'ValidationMode' : True, # bool
    'TrackTruthName' : 'TruthParticles', # str
    'TrackParticleName' : 'InDetTrackParticles', # str
    'xAODTruthLinkVector' : 'xAODTruthLinks', # str
    'isMC' : False, # bool
    'CorrectMaps' : False, # bool
    'doIP' : False, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'RegisterForContextService' : """ The flag to enforce the registration for Algorithm Context Service """,
    'ReFitterTool2' : """ ToolHandle for track fitter implementation """,
    'ReFitterTool1' : """ ToolHandle for track fitter implementation """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMonZmumu, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMonZmumu'
  pass # class IDPerfMonZmumu

class IDPerfMuonRefitter( ConfigurableAlgorithm ) :
  __slots__ = { 
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'ErrorCount' : 0, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'OutputTracksName' : 'IDMuonTracks', # str
    'ReFitterTool1' : PublicToolHandle(''), # GaudiHandle
    'ReFitterTool2' : PublicToolHandle(''), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'RegisterForContextService' : """ The flag to enforce the registration for Algorithm Context Service """,
    'ReFitterTool2' : """ ToolHandle for track fitter implementation """,
    'ReFitterTool1' : """ ToolHandle for track fitter implementation """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(IDPerfMuonRefitter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'IDPerfMuonRefitter'
  pass # class IDPerfMuonRefitter

class TRT_Electron_Monitoring_Tool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ProcessNEvents' : 0, # int
    'histoPathBase' : '', # str
    'PreScale' : 0, # int
    'TriggerChain' : '', # str
    'TriggerGroup' : '', # str
    'ManagerName' : 'AthenaMonManager', # str
    'TrigDecisionTool' : PublicToolHandle(''), # GaudiHandle
    'TriggerTranslatorTool' : PublicToolHandle(''), # GaudiHandle
    'FilterTools' : PublicToolHandleArray([]), # GaudiHandleArray
    'EnableLumi' : False, # bool
    'DetailLevel' : 1, # int
    'FileKey' : '', # str
    'DataType' : 'userDefined', # str
    'Environment' : 'noOutput', # str
    'trackName' : 'InDetTrackParticles', # str
    'electronName' : 'Electrons', # str
    'conversionName' : 'ConversionCandidate', # str
    'muonName' : 'Muons', # str
    'isOnline' : True, # bool
    'doExpert' : True, # bool
    'doShift' : True, # bool
    'doTracksMon' : True, # bool
    'doMuonMon' : True, # bool
    'doElectronMon' : True, # bool
    'doRecElectrons' : True, # bool
    'doConversions' : True, # bool
    'isEMFlag' : 'Tight', # str
    'NEtaBins' : 100, # int
    'NPhiBins' : 100, # int
    'NZRBins' : 100, # int
    'ZRMax' : 1.00000, # float
    'useTRTOnly' : False, # bool
    'NMinTRTHits' : 0, # int
    'pionTRTHitCut' : 19.0000, # float
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(TRT_Electron_Monitoring_Tool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetPerformanceMonitoring'
  def getType( self ):
      return 'TRT_Electron_Monitoring_Tool'
  pass # class TRT_Electron_Monitoring_Tool
