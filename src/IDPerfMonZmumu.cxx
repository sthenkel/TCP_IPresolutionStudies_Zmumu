//==================================================================================

//==================================================================================
// Include files...
//==================================================================================

// This files header
#include "InDetPerformanceMonitoring/IDPerfMonZmumu.h"
// Standard headers
#include <string>
#include "TTree.h"
#include "TLorentzVector.h"

#include "InDetPerformanceMonitoring/PerfMonServices.h"
#include "InDetPerformanceMonitoring/ZmumuEvent.h"

//#include "TrkFitterInterfaces/ITrackFitter.h"
#include "egammaInterfaces/IegammaTrkRefitterTool.h"

#include "EventPrimitives/EventPrimitivesHelpers.h"
//#include "xAODTruth/xAODTruthHelpers.h"
//For extrapolation

// ATLAS headers
#include "GaudiKernel/IInterface.h"
#include "StoreGate/StoreGateSvc.h"

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"

#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

#include "TrkTruthData/TrackTruthCollection.h"
#include "TrkTruthData/TrackTruthKey.h"
#include "TrkParameters/TrackParameters.h" //Contains typedef to Trk::CurvilinearParameters

#include "TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h"
#include "InDetBeamSpotService/IBeamCondSvc.h"

//==================================================================================
// Public Methods
//==================================================================================
IDPerfMonZmumu::IDPerfMonZmumu(const std::string& name,
  ISvcLocator* pSvcLocator):
  AthAlgorithm(name, pSvcLocator),
  m_isMC(false),
  m_CorrectMaps(false),
  m_TrackRefitter1(""),
  m_TrackRefitter2(""),
  m_trackToVertexTool("Reco::TrackToVertex"),
  m_trackBiasingTool("InDet::InDetTrackBiasingTool"),
  m_trackToVertexIPEstimator("Trk::TrackToVertexIPEstimator"),
  m_beamSpotSvc("BeamCondSvc",name),
  m_extrapolator("Trk::Extrapolator/AtlasExtrapolator"),
  m_validationMode(true),
  m_defaultTreeName("DefaultParams"),
  m_refit1TreeName("Refit1Params"),
  m_refit2TreeName("Refit2Params"),
  m_truthTreeName("TruthParams"),
  //  m_meStacoTreeName("MEStacoParams"),  //not existent in xAOD anymore
  m_combStacoTreeName("CombStacoParams"),
  m_combMuidTreeName("CombMuidParams"),
  m_ValidationTreeDescription("Small Tree for Zmumu fits"),
  m_defaultTreeFolder("/ZmumuValidation/default"),
  m_refit1TreeFolder("/ZmumuValidation/refit1"),
  m_refit2TreeFolder("/ZmumuValidation/refit2"),
  m_truthTreeFolder("/ZmumuValidation/truth"),
  //  m_meStacoTreeFolder("/ZmumuValidation/mestaco"),
  m_combStacoTreeFolder("/ZmumuValidation/combstaco"),
  m_defaultTree(0),
  m_refit1Tree(0),
  m_refit2Tree(0),
  m_truthTree(0),
  //  m_meStacoTree(0),   // not existent in xAOD anymore
  m_combStacoTree(0),
  m_combMuidTree(0)

{
  // Properties that are set from the python scripts.


  declareProperty("triggerChainName", m_sTriggerChainName               );
  declareProperty("OutputTracksName", m_outputTracksName = "ZmumuTracks");
  declareProperty("doIsoSelection",   m_doIsoSelection = true           );
  declareProperty("BeamCondSvc", m_beamSpotSvc);
  declareProperty ( "Extrapolator", m_extrapolator );
  declareProperty("ReFitterTool1",    m_TrackRefitter1, "ToolHandle for track fitter implementation");
  declareProperty("ReFitterTool2",    m_TrackRefitter2, "ToolHandle for track fitter implementation");

  declareProperty ("TrackToVertexTool", m_trackToVertexTool);
  declareProperty( "trackBiasingTool", m_trackBiasingTool );
  declareProperty("TrackToVertexIPEstimator", m_trackToVertexIPEstimator);
  declareProperty("ValidationMode",   m_validationMode);

  declareProperty("TrackTruthName",     m_truthName="TruthParticles");
  declareProperty("TrackParticleName",  m_trackParticleName="InDetTrackParticles");

  //  declareProperty("TrackParticleName",  m_trackTruthParticleName="TruthParticles");
  //  declareProperty("TrackParticleName",  m_trackParticleName="CombinedMuonTrackParticles");

  declareProperty("xAODTruthLinkVector",m_truthLinkVecName="xAODTruthLinks");

  declareProperty("isMC",   m_isMC = false);
  declareProperty("CorrectMaps",   m_CorrectMaps = false);
  declareProperty("doIP"       , m_doIP = false);
}



IDPerfMonZmumu::~IDPerfMonZmumu()
{}



StatusCode IDPerfMonZmumu::initialize()
{

  // Setup the services
  ISvcLocator* pxServiceLocator = serviceLocator();
  if ( pxServiceLocator != NULL ) {
      StatusCode xSC = PerfMonServices::InitialiseServices( pxServiceLocator );
      if ( xSC == !StatusCode::SUCCESS )
	{
          ATH_MSG_FATAL("Problem Initializing PerfMonServices");
	  //return PARENT::initialize();
	}
  }

  // Retrieve fitter
  if (m_TrackRefitter1.retrieve().isFailure()) {
    ATH_MSG_FATAL("Unable to retrieve " << m_TrackRefitter1 );
    return StatusCode::FAILURE;
  } else {
    ATH_MSG_INFO("Retrieved tool" << m_TrackRefitter1 );
  }

  // Retrieve the second fitter
  if (m_TrackRefitter2.retrieve().isFailure()) {
    ATH_MSG_FATAL("Unable to retrieve " << m_TrackRefitter2 );
    return StatusCode::FAILURE;
  } else {
    ATH_MSG_INFO("Retrieved tool" << m_TrackRefitter2 );
  }


  if (m_trackToVertexTool.retrieve().isFailure()) {
    ATH_MSG_FATAL("Unable to retrieve " << m_trackToVertexTool );
    return StatusCode::FAILURE;
  } else {
    ATH_MSG_INFO("Retrieved tool" << m_trackToVertexTool );
  }

  if (m_trackBiasingTool.retrieve().isFailure()) {
    ATH_MSG_FATAL("Unable to retrieve " << m_trackBiasingTool );
    return StatusCode::FAILURE;
  } else {
    ATH_MSG_INFO("Retrieved tool" << m_trackBiasingTool );
  }
  ATH_CHECK( m_beamSpotSvc.retrieve());
  ATH_CHECK(m_extrapolator.retrieve());
  if (m_doIP) {
    ATH_CHECK (m_trackToVertexIPEstimator.retrieve());
  }


  if( m_defaultTree == 0){

    m_defaultTree = new TTree(m_defaultTreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_defaultTree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_defaultTree->Branch("eventNumber"    ,  &m_evtNumber,  "eventNumber/I");
    m_defaultTree->Branch("lumi_block"     ,  &m_lumi_block,  "lumi_block/I");

    m_defaultTree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_defaultTree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_defaultTree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_defaultTree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_defaultTree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_defaultTree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_defaultTree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");
    m_defaultTree->Branch("Negative_z0_PVerr",  &m_negative_z0_PVerr,  "Negative_z0_PVerr/D");
    m_defaultTree->Branch("Negative_d0_PVerr",  &m_negative_d0_PVerr,  "Negative_d0_PVerr/D");

    m_defaultTree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_defaultTree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_defaultTree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_defaultTree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_defaultTree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_defaultTree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_defaultTree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
    m_defaultTree->Branch("Positive_z0_PVerr",  &m_positive_z0_PVerr,  "Positive_z0_PVerr/D");
    m_defaultTree->Branch("Positive_d0_PVerr",  &m_positive_d0_PVerr,  "Positive_d0_PVerr/D");
    m_defaultTree->Branch("Px",  &m_px,  "Px/D");
    m_defaultTree->Branch("Py",  &m_py,  "Py/D");
    m_defaultTree->Branch("Pz",  &m_pz,  "Pz/D");
    m_defaultTree->Branch("z0",  &m_z0,  "z0/D");
    m_defaultTree->Branch("d0",  &m_d0,  "d0/D");
  }

  if( m_refit1Tree == 0){

    m_refit1Tree = new TTree(m_refit1TreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_refit1Tree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_refit1Tree->Branch("eventNumber"    ,  &m_evtNumber,  "eventNumber/I");
    m_refit1Tree->Branch("lumi_block"     ,  &m_lumi_block,  "lumi_block/I");

    m_refit1Tree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_refit1Tree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_refit1Tree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_refit1Tree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_refit1Tree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_refit1Tree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_refit1Tree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");


    m_refit1Tree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_refit1Tree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_refit1Tree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_refit1Tree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_refit1Tree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_refit1Tree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_refit1Tree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
  }

  if( m_refit2Tree == 0){

    m_refit2Tree = new TTree(m_refit2TreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_refit2Tree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_refit2Tree->Branch("eventNumber"    ,  &m_evtNumber,  "eventNumber/I");
    m_refit2Tree->Branch("lumi_block"     ,  &m_lumi_block,  "lumi_block/I");

    m_refit2Tree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_refit2Tree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_refit2Tree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_refit2Tree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_refit2Tree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_refit2Tree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_refit2Tree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");



    m_refit2Tree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_refit2Tree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_refit2Tree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_refit2Tree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_refit2Tree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_refit2Tree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_refit2Tree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
   }

  //  if( m_meStacoTree == 0){

  //m_meStacoTree = new TTree(m_meStacoTreeName.c_str(), m_ValidationTreeDescription.c_str());

  //m_meStacoTree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
  //m_meStacoTree->Branch("eventNumber"    ,  &m_evtNumber,  "eventNumber/I");
  //m_meStacoTree->Branch("lumi_block"     ,  &m_lumi_block,  "lumi_block/I");

  //m_meStacoTree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
  //m_meStacoTree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
  //m_meStacoTree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
  //m_meStacoTree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
  //m_meStacoTree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");

  //m_meStacoTree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
  //m_meStacoTree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
  //m_meStacoTree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
  //m_meStacoTree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
  //m_meStacoTree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
  //}

  if( m_combStacoTree == 0){

    m_combStacoTree = new TTree(m_combStacoTreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_combStacoTree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_combStacoTree->Branch("eventNumber"      ,  &m_evtNumber,  "eventNumber/I");
    m_combStacoTree->Branch("lumi_block"      ,  &m_lumi_block,  "lumi_block/I");

    m_combStacoTree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_combStacoTree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_combStacoTree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_combStacoTree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_combStacoTree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_combStacoTree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_combStacoTree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");
    m_combStacoTree->Branch("Negative_z0_PVerr",  &m_negative_z0_PVerr,  "Negative_z0_PVerr/D");
    m_combStacoTree->Branch("Negative_d0_PVerr",  &m_negative_d0_PVerr,  "Negative_d0_PVerr/D");

    m_combStacoTree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_combStacoTree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_combStacoTree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_combStacoTree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_combStacoTree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_combStacoTree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_combStacoTree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
    m_combStacoTree->Branch("Positive_z0_PVerr",  &m_positive_z0_PVerr,  "Positive_z0_PVerr/D");
    m_combStacoTree->Branch("Positive_d0_PVerr",  &m_positive_d0_PVerr,  "Positive_d0_PVerr/D");

  }


  if( m_combMuidTree == 0){

    m_combMuidTree = new TTree(m_combMuidTreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_combMuidTree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_combMuidTree->Branch("eventNumber"      ,  &m_evtNumber,  "eventNumber/I");
    m_combMuidTree->Branch("lumi_block"      ,  &m_lumi_block,  "lumi_block/I");

    m_combMuidTree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_combMuidTree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_combMuidTree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_combMuidTree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_combMuidTree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_combMuidTree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_combMuidTree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");



    m_combMuidTree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_combMuidTree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_combMuidTree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_combMuidTree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_combMuidTree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_combMuidTree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_combMuidTree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
  }

  if( m_isMC && m_truthTree == 0){

    m_truthTree = new TTree(m_truthTreeName.c_str(), m_ValidationTreeDescription.c_str());

    m_truthTree->Branch("runNumber"      ,  &m_runNumber,  "runNumber/I");
    m_truthTree->Branch("eventNumber"      ,  &m_evtNumber,  "eventNumber/I");
    m_truthTree->Branch("lumi_block"      ,  &m_lumi_block,  "lumi_block/I");

    m_truthTree->Branch("Negative_Px",  &m_negative_px,  "Negative_Px/D");
    m_truthTree->Branch("Negative_Py",  &m_negative_py,  "Negative_Py/D");
    m_truthTree->Branch("Negative_Pz",  &m_negative_pz,  "Negative_Pz/D");
    m_truthTree->Branch("Negative_z0",  &m_negative_z0,  "Negative_z0/D");
    m_truthTree->Branch("Negative_d0",  &m_negative_d0,  "Negative_d0/D");
    m_truthTree->Branch("Negative_z0_err",  &m_negative_z0_err,  "Negative_z0_err/D");
    m_truthTree->Branch("Negative_d0_err",  &m_negative_d0_err,  "Negative_d0_err/D");
    m_truthTree->Branch("Negative_z0_PVerr",  &m_negative_z0_PVerr,  "Negative_z0_PVerr/D");
    m_truthTree->Branch("Negative_d0_PVerr",  &m_negative_d0_PVerr,  "Negative_d0_PVerr/D");

    m_truthTree->Branch("Positive_Px",  &m_positive_px,  "Positive_Px/D");
    m_truthTree->Branch("Positive_Py",  &m_positive_py,  "Positive_Py/D");
    m_truthTree->Branch("Positive_Pz",  &m_positive_pz,  "Positive_Pz/D");
    m_truthTree->Branch("Positive_z0",  &m_positive_z0,  "Positive_z0/D");
    m_truthTree->Branch("Positive_d0",  &m_positive_d0,  "Positive_d0/D");
    m_truthTree->Branch("Positive_z0_err",  &m_positive_z0_err,  "Positive_z0_err/D");
    m_truthTree->Branch("Positive_d0_err",  &m_positive_d0_err,  "Positive_d0_err/D");
    m_truthTree->Branch("Positive_z0_PVerr",  &m_positive_z0_PVerr,  "Positive_z0_PVerr/D");
    m_truthTree->Branch("Positive_d0_PVerr",  &m_positive_d0_PVerr,  "Positive_d0_PVerr/D");
    m_truthTree->Branch("d0_rec_truth",  &m_d0_rec_truth,  "d0_rec_truth/D");
    m_truthTree->Branch("z0_rec_truth",  &m_z0_rec_truth,  "z0_rec_truth/D");
    m_truthTree->Branch("Px",  &m_px,  "Px/D");
    m_truthTree->Branch("Py",  &m_py,  "Py/D");
    m_truthTree->Branch("Pz",  &m_pz,  "Pz/D");
    m_truthTree->Branch("z0",  &m_z0,  "z0/D");
    m_truthTree->Branch("d0",  &m_d0,  "d0/D");
  }

    // now register the Trees
  ITHistSvc* tHistSvc = 0;
  if (service("THistSvc",tHistSvc).isFailure()){
    ATH_MSG_ERROR("initialize() Could not find Hist Service -> Switching ValidationMode Off !");
    m_validationMode = false;
  }

  if ((tHistSvc->regTree(m_defaultTreeFolder, m_defaultTree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_defaultTree; m_defaultTree = 0;
    m_validationMode = false;
  }

  if ((tHistSvc->regTree(m_refit1TreeFolder, m_refit1Tree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_refit1Tree; m_refit1Tree = 0;
    m_validationMode = false;
  }
  if ((tHistSvc->regTree(m_refit2TreeFolder, m_refit2Tree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_refit2Tree; m_refit2Tree = 0;
    m_validationMode = false;
  }

  //  if ((tHistSvc->regTree(m_meStacoTreeFolder, m_meStacoTree)).isFailure() ) {
  //ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
  //delete m_meStacoTree; m_meStacoTree = 0;
  //m_validationMode = false;
  //}

  if ((tHistSvc->regTree(m_combStacoTreeFolder, m_combStacoTree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_combStacoTree; m_combStacoTree = 0;
    m_validationMode = false;
  }

  if ((tHistSvc->regTree(m_combMuidTreeFolder, m_combMuidTree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_combMuidTree; m_combMuidTree = 0;
    m_validationMode = false;
  }

  if (m_isMC) {
  if ((tHistSvc->regTree(m_truthTreeFolder, m_truthTree)).isFailure() ) {
    ATH_MSG_ERROR("initialize() Could not register the validation Tree -> Switching ValidationMode Off !");
    delete m_truthTree; m_truthTree = 0;
    m_validationMode = false;
  }
  }

  return StatusCode::SUCCESS;
}


void IDPerfMonZmumu::RegisterHistograms()
{
  return;
}


StatusCode IDPerfMonZmumu::execute()
{

  ATH_MSG_DEBUG("Retrieving event info.");
  const EventInfo * eventInfo;
  if (evtStore()->retrieve(eventInfo).isFailure())
    ATH_MSG_ERROR("Could not retrieve event info.");
  else
  {
    m_runNumber = eventInfo->event_ID()->run_number();
    m_evtNumber = eventInfo->event_ID()->event_number();
    m_lumi_block = eventInfo->event_ID()->lumi_block();
  }

  //Fill Staco muon parameters only
  m_xZmm.setContainer(PerfMonServices::MUON_COLLECTION);
  m_xZmm.Reco();
  if ( m_xZmm.EventPassed() ) {
    //fill Combined Staco parameters
    const xAOD::Muon* muon_pos = m_xZmm.getCombMuon(m_xZmm.getPosMuon(ZmumuEvent::CB));
    const xAOD::Muon* muon_neg = m_xZmm.getCombMuon(m_xZmm.getNegMuon(ZmumuEvent::CB));
    if (!muon_pos || !muon_neg) {
      ATH_MSG_WARNING("CB Staco Muons missing!");
    } else {
      //       FillRecParameters(muon_pos->combinedMuonTrackParticle()->originalTrack(), muon_pos->combinedMuonTrackParticle()->charge());
      //       FillRecParameters(muon_neg->combinedMuonTrackParticle()->originalTrack(), muon_neg->combinedMuonTrackParticle()->charge());
      //      std::cout <<">>>>>>>>>>>>>>>>>>> filling comb muons 1st  <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
      FillRecParameters(muon_pos->trackParticle(xAOD::Muon::CombinedTrackParticle), muon_pos->trackParticle(xAOD::Muon::CombinedTrackParticle)->charge());
      FillRecParameters(muon_neg->trackParticle(xAOD::Muon::CombinedTrackParticle), muon_neg->trackParticle(xAOD::Muon::CombinedTrackParticle)->charge());
      m_combStacoTree->Fill();

      //      std::cout <<">>>>>>>>>>>>>>>>>>> filling default params (1) <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
      FillRecParameters(muon_pos->trackParticle(xAOD::Muon::InnerDetectorTrackParticle), muon_pos->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->charge());
      //      std::cout <<">>>>>>>>>>>>>>>>>>> filling default params (2) <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
      FillRecParameters(muon_neg->trackParticle(xAOD::Muon::InnerDetectorTrackParticle), muon_neg->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->charge());
      m_defaultTree->Fill();

      //fill truth first in case no truth match found
      if (m_isMC) {
	if(muon_pos->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->charge() == 1){
	  if (FillTruthParameters(muon_pos->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)).isFailure()){
	    ATH_MSG_WARNING("Failed to fill truth parameters - skipping event");
	    return StatusCode::SUCCESS;
	  }
	}
	if(muon_neg->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)->charge() == -1){
	  if (FillTruthParameters(muon_neg->trackParticle(xAOD::Muon::InnerDetectorTrackParticle)).isFailure()){
	    ATH_MSG_WARNING("Failed to fill truth parameters - skipping event");
	    return StatusCode::SUCCESS;
	  }
	}

	m_truthTree->Fill();
      }


    }

  }

  if ( !m_xZmm.EventPassed() ) {
    //failed cuts, continue to next event
    return StatusCode::SUCCESS;
  }
  //const std::string region = m_xZmm.getRegion();


  //  //  const xAOD::TrackParticle* p1 = m_xZmm.getIDTrack(m_xZmm.getPosMuon(ZmumuEvent::ID));
  //  //  const xAOD::TrackParticle* p2 = m_xZmm.getIDTrack(m_xZmm.getNegMuon(ZmumuEvent::ID));
  //  DataVector<xAOD::TrackParticle>* muonTrks = new DataVector<xAOD::TrackParticle>(SG::OWN_ELEMENTS);


  ////  if (!p1 || !p2) {
  ////     ATH_MSG_WARNING("Trackparticle missing!  Skipping Event");
  ////     return StatusCode::SUCCESS;
  ////  }

  //  const xAOD::TrackParticle* defaultMuonTrk1 = 0;
  //  const xAOD::TrackParticle* defaultMuonTrk2 = 0;


  //  StatusCode fitStatus;
  //save default and refit track parameters
  //  if( p1 ) {
  //    defaultMuonTrk1 = p1;//const_cast<xAOD::TrackParticle*>(p1);
  //  }

  //  if( p2 ) {
    //    defaultMuonTrk2 = new Trk::Track(*p2->track());
  //    defaultMuonTrk2 = p2;//const_cast<xAOD::TrackParticle*>(p2);
  //  }

  //save tracks to storegrate	/
  //  muonTrks->push_back(const_cast<xAOD::TrackParticle*>(defaultMuonTrk1));
  //  muonTrks->push_back(const_cast<xAOD::TrackParticle*>(defaultMuonTrk2));

  //  StatusCode sc = evtStore()->record(muonTrks, m_outputTracksName, false);
  //  if (sc.isFailure()) {
  //    if (msgLvl(MSG::WARNING)) msg(MSG::WARNING) << "Failed storing " << m_outputTracksName << endreq;
  //  }
  //  else{
  //    if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Stored "<< muonTrks->size() << " " << m_outputTracksName <<" into StoreGate" << endreq;
  //  }

  //fill truth first in case no truth match found
  ////  if (m_isMC) {

  ////     if (FillTruthParameters(p1).isFailure()){
  ////	ATH_MSG_WARNING("Failed to fill truth parameters - skipping event");
  ////  	return StatusCode::SUCCESS;
  ////     }
  ////     if (FillTruthParameters(p2).isFailure()){
  ////	ATH_MSG_WARNING("Failed to fill truth parameters - skipping event");
  ////	return StatusCode::SUCCESS;
  ////     }
  ////     m_truthTree->Fill();
  ////  }

  //fill default ID parameters
  //    if (muonTrks->size()<2){
  //      ATH_MSG_WARNING("Default muon tracks are missing!");
  //    }else{
  //      std::cout <<">>>>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
  //    FillRecParameters(defaultMuonTrk1, p1->charge());
  //        std::cout <<">>>>>>>>>>>>>>>>>>> filling default params (2) <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
  //        FillRecParameters(defaultMuonTrk2, p2->charge());
  //        m_defaultTree->Fill();
  //        }

  //fill Combined parameters
  //       const xAOD::Muon* muon_pos = m_xZmm.getCombMuon(m_xZmm.getPosMuon(ZmumuEvent::CB));
  //    const xAOD::Muon* muon_neg = m_xZmm.getCombMuon(m_xZmm.getNegMuon(ZmumuEvent::CB));

  //    if (!muon_pos || !muon_neg) {
  //      ATH_MSG_WARNING("CB Muons missing!");
  //    } else {
  //      std::cout <<">>>>>>>>>>>>>>>>>>> filling comb muons params  <<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
  //      FillRecParameters(muon_pos->trackParticle(xAOD::Muon::CombinedTrackParticle), muon_pos->trackParticle(xAOD::Muon::CombinedTrackParticle)->charge());
  //      FillRecParameters(muon_neg->trackParticle(xAOD::Muon::CombinedTrackParticle), muon_neg->trackParticle(xAOD::Muon::CombinedTrackParticle)->charge());
  //      m_combMuidTree->Fill();
  //    }

  return StatusCode::SUCCESS;
}


void IDPerfMonZmumu::FillRecParameters(const xAOD::TrackParticle* trackp, double charge)
{
  if (!trackp || !trackp->vertex()){
    ATH_MSG_WARNING("Empty Trackparticle. Skipping.");
    return;
  }
  //  std::cout << "VertexType : "<< trackp->vertex()->vertexType() << std::endl;
  //  m_trackBiasingTool->applyCorrection(&trackp)

  auto trkPerigee = &trackp->perigeeParameters();

  if(m_CorrectMaps) {
    xAOD::TrackParticle* newTrack = nullptr;
    if ( m_trackBiasingTool->correctedCopy( *trackp, newTrack ) == CP::CorrectionCode::Error) {
      // report and handle the error
    }
    auto trkPerigee = &newTrack->perigeeParameters();
  }

  //  xAOD::TrackParticle* newTrack = nullptr;
  //  if(m_CorrectMaps) {
  //    if ( m_trackBiasingTool->correctedCopy( *trackp, newTrack ) == CP::CorrectionCode::Error) {
  //    }else{
  //    auto trkPerigee = &newTrack->perigeeParameters();
  //    }
  //  }

  //  const Trk::Perigee* trkPerigee = trackp->perigeeParameters();
  //  auto trkPerigee = &trackp->perigeeParameters();
  //  const AmgSymMatrix(5)* covariance = trkPerigee ? trkPerigee->covariance() : NULL;
  //  if (covariance == NULL) {
  //    if (msgLvl(MSG::WARNING)) msg(MSG::WARNING) << "No measured perigee parameters assigned to the track" << endreq;
  //  }

  double px = 0;
  double py = 0;
  double pz = 0;
  double d0 = 0;
  double z0 = 0;
  double d0res = 0;
  double PVd0res = 0;
  double z0res = 0;
  double PVz0res = 0;

  // if(trkPerigee){
  //   double qOverP   = trkPerigee->parameters()[Trk::qOverP];
  //   if (qOverP) {
  //     px = trkPerigee->momentum().x();
  //     py = trkPerigee->momentum().y();
  //     pz = trkPerigee->momentum().z();
  //     if(!m_doIP){
  // 	d0 = trkPerigee->parameters()[Trk::d0];
  // 	d0res = -99;
  // 	z0 = trkPerigee->parameters()[Trk::z0];
  // 	z0res = -99;
  //     }
  //   }
  //   //    std::cout << "trkPerigee -- px " << px << " py : " << py << " pz : " << pz << " d0 : "<< d0 << "  z0 : "<< z0 << std::endl;
  // }


  //   auto atBL =  m_trackToVertexTool->trackAtBeamline( *trackp );

  //   if (atBL){
  // double qOverP   = atBL->parameters()[Trk::qOverP];
  // if(qOverP){
  //   px = atBL->momentum().x();
  //   py = atBL->momentum().y();
  //   pz = atBL->momentum().z();
  //   if(!m_doIP){
  //	d0 = atBL->parameters()[Trk::d0];
  //	d0res = -99;
  //	z0 = atBL->parameters()[Trk::z0];
  //	z0res = -99;
  //   }
  // }
  //        std::cout << "atBL -- px " << px << " py : " << py << " pz : " << pz << " d0 : "<< d0 << "  z0 : "<< z0 << std::endl;
	 //  }//atbl



 px = trackp->p4().Px();
 py = trackp->p4().Py();
 pz = trackp->p4().Pz();
 if(m_doIP){
   const Trk::ImpactParametersAndSigma* iPandSigma(NULL);
   ATH_MSG_INFO("using the trackToVertexIPEstimator");
   iPandSigma = m_trackToVertexIPEstimator->estimate(trackp,trackp->vertex(),true);
   if( iPandSigma==0 ){
     ATH_MSG_WARNING ("trackToVertexIPEstimator failed !");
     return;
   }
   else{
     ATH_MSG_DEBUG("trackToVertexIPEstimator success !");
     d0 = iPandSigma->IPd0;
     PVd0res = iPandSigma->PVsigmad0;
     d0res = iPandSigma->sigmad0;
     z0 = iPandSigma->IPz0;
     PVz0res = iPandSigma->PVsigmaz0;
     z0res = iPandSigma->sigmaz0;
   }
 }else{
   d0 = trackp->d0();
   z0 = trackp->z0();
   d0res = -99;
   z0res = -99;
 }


  if (charge == 1) {
    m_positive_px = px;
    m_positive_py = py;
    m_positive_pz = pz;
    m_positive_z0 = z0;
    m_positive_z0_err = z0res;
    m_positive_z0_PVerr = PVz0res;
    m_positive_d0 = d0;
    m_positive_d0_err = d0res;
    m_positive_d0_PVerr = PVd0res;
    m_px = px;
    m_py = py;
    m_pz = pz;
    m_z0 = z0;
    m_d0 = d0;
    ATH_MSG_DEBUG("(Filled charge == 1 ) (reco)-> px : "<< px <<" py: "<<py <<" pz: "<<pz <<" d0: "<<m_positive_d0 << " d0res : "<< d0res << " PVd0res : "<< PVd0res <<" z0: "<< m_positive_z0 << " z0res : " << z0res <<  " PVz0res : "<< PVz0res );

  } else  if (charge == -1) {
    m_negative_px = px;
    m_negative_py = py;
    m_negative_pz = pz;
    m_negative_z0 = z0;
    m_negative_z0_err = z0res;
    m_negative_z0_PVerr = PVz0res;
    m_negative_d0 = d0;
    m_negative_d0_err = d0res;
    m_negative_d0_PVerr = PVd0res;
    m_px = px;
    m_py = py;
    m_pz = pz;
    m_z0 = z0;
    m_d0 = d0;
    ATH_MSG_DEBUG("(Filled charge == -1 ) (reco)-> px : "<< px <<" py: "<<py <<" pz: "<<pz <<" d0: "<<m_negative_d0 << " d0res : "<< d0res << " PVd0res : "<< PVd0res <<" z0: "<< m_negative_z0 << " z0res : " << z0res <<  " PVz0res : "<< PVz0res );

  }

  return;
}


StatusCode IDPerfMonZmumu::FillTruthParameters(const xAOD::TrackParticle* trackParticle)
{
  if (!trackParticle || !trackParticle->vertex()){
    ATH_MSG_WARNING("Empty Trackparticle. Skipping.");
    return StatusCode::FAILURE;
  }
  const xAOD::TruthParticle* particle = getTruthParticle( *trackParticle );
  double charge = 0;
  if(particle->pdgId() == 13) charge = -1.;
  else if(particle->pdgId() == -13) charge = 1.;
  if( particle->isNeutral() ) return StatusCode::FAILURE;
  const Amg::Vector3D momentum(particle->px(), particle->py(), particle->pz());
  const xAOD::TruthVertex * ptruthVertex(0);
  ptruthVertex=particle->prodVtx();
  if (!ptruthVertex){
    ATH_MSG_DEBUG("A production vertex pointer was retrieved, but it is NULL");
    return StatusCode::FAILURE;
  }

  const auto xPos=ptruthVertex->x();
  const auto yPos=ptruthVertex->y();
  const auto z_truth=ptruthVertex->z();

  const Amg::Vector3D position(xPos, yPos, z_truth);
  //  const float prodR_truth = std::sqrt(xPos*xPos+ yPos*yPos);
  //delete ptruthVertex;ptruthVertex=0;
  const Trk::CurvilinearParameters cParameters(position, momentum, charge);

  Trk::PerigeeSurface persf( m_beamSpotSvc->beamPos() );

  const Trk::TrackParameters* tP = m_extrapolator->extrapolate(cParameters,persf, Trk::anyDirection, false);


  double px = 0;
  double py = 0;
  double pz = 0;
  double d0 = 0;
  double d0res = 0;
  double PVd0res = 0;
  double z0 = 0;
  double z0res = 0;
  double PVz0res = 0;


  double d0recoPos =  m_positive_d0;
  double z0recoPos =  m_positive_z0;
  double d0recoNeg =  m_negative_d0;
  double z0recoNeg =  m_negative_z0;

  ATH_MSG_DEBUG("reco IPs (pos): > d0 : "<<d0recoPos << " z0: " << z0recoPos << " trackp z0 : " << trackParticle->z0() << " trackp d0 : " << trackParticle->d0());
  ATH_MSG_DEBUG("reco IPs (neg): > d0 : "<<d0recoNeg << " z0: " << z0recoNeg << " trackp z0 : " << trackParticle->z0() <<" trackp d0 : " << trackParticle->d0() );

  if (tP){
    double qOverP_truth = tP->parameters()[Trk::qOverP];
    if( qOverP_truth ){

      px = tP->momentum().x();
      py = tP->momentum().y();
      pz = tP->momentum().z();
      d0 = tP->parameters()[Trk::d0];
      z0 = tP->parameters()[Trk::z0];

      ATH_MSG_DEBUG("cand perig HEP particle (truth) px : "<< tP->momentum().x());
      ATH_MSG_DEBUG("cand perig HEP particle (truth) py : "<< tP->momentum().y());
      ATH_MSG_DEBUG("cand perig HEP particle (truth) pz : "<< tP->momentum().z());
      ATH_MSG_DEBUG("cand perig HEP particle (truth) d0 : "<< tP->parameters()[Trk::d0]);
      ATH_MSG_DEBUG("cand perig HEP particle (truth) z0 : "<< tP->parameters()[Trk::z0]);
      delete tP;tP=0;
    }
  }


  if (charge == 1) {
    m_positive_px = px;
    m_positive_py = py;
    m_positive_pz = pz;
    m_positive_z0 = z0recoPos -z0;
    //    m_positive_z0 = z0;

    m_positive_z0_err = z0res;
    m_positive_z0_PVerr = PVz0res;
    m_positive_d0 = d0recoPos -d0;
    //    m_positive_d0 = d0;

    m_positive_d0_err = d0res;
    m_positive_d0_PVerr = PVd0res;
    m_px = px;
    m_py = py;
    m_pz = pz;
    m_z0 = z0;
    m_d0 = d0;
    //    m_d0_rec_truth = d0recoPos -d0;
    //    m_z0_rec_truth = z0recoPos -z0;
    ATH_MSG_DEBUG("(Filled charge == 1 ) (truth)-> px : "<< m_positive_px <<" py: "<<m_positive_py <<" pz: "<<m_positive_pz <<" d0: "<<m_positive_d0 << " z0: "<< m_positive_z0);

  } else  if (charge == -1) {
    m_negative_px = px;
    m_negative_py = py;
    m_negative_pz = pz;
    m_negative_z0 = z0recoNeg-z0;
    //    m_negative_z0 = z0;

    m_negative_z0_err = z0res;
    m_negative_z0_PVerr = PVz0res;
    m_negative_d0 = d0recoNeg-d0;
    //    m_negative_d0 = d0;

    m_negative_d0_err = d0res;
    m_negative_d0_PVerr = PVd0res;

    m_px = px;
    m_py = py;
    m_pz = pz;
    m_z0 = z0;
    m_d0 = d0;
    //    m_d0_rec_truth = d0recoNeg -d0;
    //    m_z0_rec_truth = z0recoNeg -z0;
    ATH_MSG_DEBUG("(Filled charge == -1 ) (truth)-> px : "<< m_negative_px <<" py: "<<m_negative_py <<" pz: "<<m_negative_pz <<" d0: "<<m_negative_d0 << " z0: "<< m_negative_z0);
  }
    return StatusCode::SUCCESS;
}


const xAOD::TruthParticle* IDPerfMonZmumu::getTruthParticle( const xAOD::IParticle& p ) {


  /// A convenience type declaration
  typedef ElementLink< xAOD::TruthParticleContainer > Link_t;

  /// A static accessor for the information
  static SG::AuxElement::ConstAccessor< Link_t > acc( "truthParticleLink" );

  // Check if such a link exists on the object:
  if( ! acc.isAvailable( p ) ) {
    return 0;
  }

  // Get the link:
  const Link_t& link = acc( p );

  // Check if the link is valid:
  if( ! link.isValid() ) {
    return 0;
  }

  // Everything has passed, let's return the pointer:
  return *link;
}

StatusCode IDPerfMonZmumu::finalize()
{
  return StatusCode::SUCCESS;
}
