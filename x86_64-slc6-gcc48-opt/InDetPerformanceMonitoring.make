#-- start of make_header -----------------

#====================================
#  Library InDetPerformanceMonitoring
#
#   Generated Wed Apr 27 05:22:24 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoring_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoring_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoring

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoring = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoring.make
cmt_local_tagfile_InDetPerformanceMonitoring = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoring.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoring = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoring = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoring)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoring)

ifdef cmt_InDetPerformanceMonitoring_has_target_tag

cmt_final_setup_InDetPerformanceMonitoring = $(bin)setup_InDetPerformanceMonitoring.make
cmt_dependencies_in_InDetPerformanceMonitoring = $(bin)dependencies_InDetPerformanceMonitoring.in
#cmt_final_setup_InDetPerformanceMonitoring = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoring_makefile = $(bin)InDetPerformanceMonitoring.make

else

cmt_final_setup_InDetPerformanceMonitoring = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoring = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoring = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoring_makefile = $(bin)InDetPerformanceMonitoring.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoring :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoring'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoring/
#InDetPerformanceMonitoring::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

InDetPerformanceMonitoringlibname   = $(bin)$(library_prefix)InDetPerformanceMonitoring$(library_suffix)
InDetPerformanceMonitoringlib       = $(InDetPerformanceMonitoringlibname).a
InDetPerformanceMonitoringstamp     = $(bin)InDetPerformanceMonitoring.stamp
InDetPerformanceMonitoringshstamp   = $(bin)InDetPerformanceMonitoring.shstamp

InDetPerformanceMonitoring :: dirs  InDetPerformanceMonitoringLIB
	$(echo) "InDetPerformanceMonitoring ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#InDetPerformanceMonitoringLIB :: $(InDetPerformanceMonitoringlib) $(InDetPerformanceMonitoringshstamp)
InDetPerformanceMonitoringLIB :: $(InDetPerformanceMonitoringshstamp)
	$(echo) "InDetPerformanceMonitoring : library ok"

$(InDetPerformanceMonitoringlib) :: $(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(InDetPerformanceMonitoringlib) $?
	$(lib_silent) $(ranlib) $(InDetPerformanceMonitoringlib)
	$(lib_silent) cat /dev/null >$(InDetPerformanceMonitoringstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(InDetPerformanceMonitoringlibname).$(shlibsuffix) :: $(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o $(use_requirements) $(InDetPerformanceMonitoringstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o $(InDetPerformanceMonitoring_shlibflags)
	$(lib_silent) cat /dev/null >$(InDetPerformanceMonitoringstamp) && \
	  cat /dev/null >$(InDetPerformanceMonitoringshstamp)

$(InDetPerformanceMonitoringshstamp) :: $(InDetPerformanceMonitoringlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(InDetPerformanceMonitoringlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(InDetPerformanceMonitoringstamp) && \
	  cat /dev/null >$(InDetPerformanceMonitoringshstamp) ; fi

InDetPerformanceMonitoringclean ::
	$(cleanup_echo) objects InDetPerformanceMonitoring
	$(cleanup_silent) /bin/rm -f $(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o) $(patsubst %.o,%.dep,$(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o) $(patsubst %.o,%.d.stamp,$(bin)InDetPerformanceMonitoring_entries.o $(bin)InDetPerformanceMonitoring_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf InDetPerformanceMonitoring_deps InDetPerformanceMonitoring_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
InDetPerformanceMonitoringinstallname = $(library_prefix)InDetPerformanceMonitoring$(library_suffix).$(shlibsuffix)

InDetPerformanceMonitoring :: InDetPerformanceMonitoringinstall ;

install :: InDetPerformanceMonitoringinstall ;

InDetPerformanceMonitoringinstall :: $(install_dir)/$(InDetPerformanceMonitoringinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(InDetPerformanceMonitoringinstallname) :: $(bin)$(InDetPerformanceMonitoringinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetPerformanceMonitoringinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##InDetPerformanceMonitoringclean :: InDetPerformanceMonitoringuninstall

uninstall :: InDetPerformanceMonitoringuninstall ;

InDetPerformanceMonitoringuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetPerformanceMonitoringinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetPerformanceMonitoring_entries.d

$(bin)$(binobj)InDetPerformanceMonitoring_entries.d :

$(bin)$(binobj)InDetPerformanceMonitoring_entries.o : $(cmt_final_setup_InDetPerformanceMonitoring)

$(bin)$(binobj)InDetPerformanceMonitoring_entries.o : $(src)components/InDetPerformanceMonitoring_entries.cxx
	$(cpp_echo) $(src)components/InDetPerformanceMonitoring_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoring_pp_cppflags) $(lib_InDetPerformanceMonitoring_pp_cppflags) $(InDetPerformanceMonitoring_entries_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoring_cppflags) $(lib_InDetPerformanceMonitoring_cppflags) $(InDetPerformanceMonitoring_entries_cppflags) $(InDetPerformanceMonitoring_entries_cxx_cppflags) -I../src/components $(src)components/InDetPerformanceMonitoring_entries.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoring_dependencies.make : $(InDetPerformanceMonitoring_entries_cxx_dependencies)

$(bin)InDetPerformanceMonitoring_dependencies.make : $(src)components/InDetPerformanceMonitoring_entries.cxx

$(bin)$(binobj)InDetPerformanceMonitoring_entries.o : $(InDetPerformanceMonitoring_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/InDetPerformanceMonitoring_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoring_pp_cppflags) $(lib_InDetPerformanceMonitoring_pp_cppflags) $(InDetPerformanceMonitoring_entries_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoring_cppflags) $(lib_InDetPerformanceMonitoring_cppflags) $(InDetPerformanceMonitoring_entries_cppflags) $(InDetPerformanceMonitoring_entries_cxx_cppflags) -I../src/components $(src)components/InDetPerformanceMonitoring_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetPerformanceMonitoring_load.d

$(bin)$(binobj)InDetPerformanceMonitoring_load.d :

$(bin)$(binobj)InDetPerformanceMonitoring_load.o : $(cmt_final_setup_InDetPerformanceMonitoring)

$(bin)$(binobj)InDetPerformanceMonitoring_load.o : $(src)components/InDetPerformanceMonitoring_load.cxx
	$(cpp_echo) $(src)components/InDetPerformanceMonitoring_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoring_pp_cppflags) $(lib_InDetPerformanceMonitoring_pp_cppflags) $(InDetPerformanceMonitoring_load_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoring_cppflags) $(lib_InDetPerformanceMonitoring_cppflags) $(InDetPerformanceMonitoring_load_cppflags) $(InDetPerformanceMonitoring_load_cxx_cppflags) -I../src/components $(src)components/InDetPerformanceMonitoring_load.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoring_dependencies.make : $(InDetPerformanceMonitoring_load_cxx_dependencies)

$(bin)InDetPerformanceMonitoring_dependencies.make : $(src)components/InDetPerformanceMonitoring_load.cxx

$(bin)$(binobj)InDetPerformanceMonitoring_load.o : $(InDetPerformanceMonitoring_load_cxx_dependencies)
	$(cpp_echo) $(src)components/InDetPerformanceMonitoring_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoring_pp_cppflags) $(lib_InDetPerformanceMonitoring_pp_cppflags) $(InDetPerformanceMonitoring_load_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoring_cppflags) $(lib_InDetPerformanceMonitoring_cppflags) $(InDetPerformanceMonitoring_load_cppflags) $(InDetPerformanceMonitoring_load_cxx_cppflags) -I../src/components $(src)components/InDetPerformanceMonitoring_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoring.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library InDetPerformanceMonitoring
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)InDetPerformanceMonitoring$(library_suffix).a $(library_prefix)InDetPerformanceMonitoring$(library_suffix).$(shlibsuffix) InDetPerformanceMonitoring.stamp InDetPerformanceMonitoring.shstamp
#-- end of cleanup_library ---------------
