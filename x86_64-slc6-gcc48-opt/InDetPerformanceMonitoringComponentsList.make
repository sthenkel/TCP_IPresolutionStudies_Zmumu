#-- start of make_header -----------------

#====================================
#  Document InDetPerformanceMonitoringComponentsList
#
#   Generated Wed Apr 27 05:23:02 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoringComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoringComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoringComponentsList

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringComponentsList = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringComponentsList.make
cmt_local_tagfile_InDetPerformanceMonitoringComponentsList = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringComponentsList = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoringComponentsList = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoringComponentsList)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoringComponentsList)

ifdef cmt_InDetPerformanceMonitoringComponentsList_has_target_tag

cmt_final_setup_InDetPerformanceMonitoringComponentsList = $(bin)setup_InDetPerformanceMonitoringComponentsList.make
cmt_dependencies_in_InDetPerformanceMonitoringComponentsList = $(bin)dependencies_InDetPerformanceMonitoringComponentsList.in
#cmt_final_setup_InDetPerformanceMonitoringComponentsList = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringComponentsListsetup.make
cmt_local_InDetPerformanceMonitoringComponentsList_makefile = $(bin)InDetPerformanceMonitoringComponentsList.make

else

cmt_final_setup_InDetPerformanceMonitoringComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoringComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoringComponentsList = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoringComponentsList_makefile = $(bin)InDetPerformanceMonitoringComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoringComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoringComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoringComponentsList/
#InDetPerformanceMonitoringComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = InDetPerformanceMonitoring.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libInDetPerformanceMonitoring.$(shlibsuffix)

InDetPerformanceMonitoringComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: InDetPerformanceMonitoringComponentsListinstall
InDetPerformanceMonitoringComponentsListinstall :: InDetPerformanceMonitoringComponentsList

uninstall :: InDetPerformanceMonitoringComponentsListuninstall
InDetPerformanceMonitoringComponentsListuninstall :: InDetPerformanceMonitoringComponentsListclean

InDetPerformanceMonitoringComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoringComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringComponentsListclean ::
#-- end of cleanup_header ---------------
