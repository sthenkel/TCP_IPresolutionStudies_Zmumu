#-- start of make_header -----------------

#====================================
#  Document InDetPerformanceMonitoringConf
#
#   Generated Wed Apr 27 05:22:54 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoringConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoringConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoringConf

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringConf = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringConf.make
cmt_local_tagfile_InDetPerformanceMonitoringConf = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringConf = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoringConf = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoringConf)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoringConf)

ifdef cmt_InDetPerformanceMonitoringConf_has_target_tag

cmt_final_setup_InDetPerformanceMonitoringConf = $(bin)setup_InDetPerformanceMonitoringConf.make
cmt_dependencies_in_InDetPerformanceMonitoringConf = $(bin)dependencies_InDetPerformanceMonitoringConf.in
#cmt_final_setup_InDetPerformanceMonitoringConf = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringConfsetup.make
cmt_local_InDetPerformanceMonitoringConf_makefile = $(bin)InDetPerformanceMonitoringConf.make

else

cmt_final_setup_InDetPerformanceMonitoringConf = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoringConf = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoringConf = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoringConf_makefile = $(bin)InDetPerformanceMonitoringConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoringConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoringConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoringConf/
#InDetPerformanceMonitoringConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: InDetPerformanceMonitoringConf InDetPerformanceMonitoringConfclean

confpy  := InDetPerformanceMonitoringConf.py
conflib := $(bin)$(library_prefix)InDetPerformanceMonitoring.$(shlibsuffix)
confdb  := InDetPerformanceMonitoring.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

InDetPerformanceMonitoringConf :: InDetPerformanceMonitoringConfinstall

install :: InDetPerformanceMonitoringConfinstall

InDetPerformanceMonitoringConfinstall : /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring/$(confpy)
	@echo "Installing /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring in /afs/cern.ch/work/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring /afs/cern.ch/work/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InstallArea/python ; \

/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring/$(confpy) : $(conflib) /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)InDetPerformanceMonitoring.$(shlibsuffix)

/afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring:
	@ if [ ! -d /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring ] ; then mkdir -p /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring ; fi ;

InDetPerformanceMonitoringConfclean :: InDetPerformanceMonitoringConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring/$(confpy) /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring/$(confdb)

uninstall :: InDetPerformanceMonitoringConfuninstall

InDetPerformanceMonitoringConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/work/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InstallArea/python
libInDetPerformanceMonitoring_so_dependencies = ../x86_64-slc6-gcc48-opt/libInDetPerformanceMonitoring.so
#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoringConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringConfclean ::
#-- end of cleanup_header ---------------
