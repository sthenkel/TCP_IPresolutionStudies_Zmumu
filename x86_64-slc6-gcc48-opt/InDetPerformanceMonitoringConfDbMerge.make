#-- start of make_header -----------------

#====================================
#  Document InDetPerformanceMonitoringConfDbMerge
#
#   Generated Wed Apr 27 05:23:02 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoringConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoringConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoringConfDbMerge

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringConfDbMerge.make
cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoringConfDbMerge)

ifdef cmt_InDetPerformanceMonitoringConfDbMerge_has_target_tag

cmt_final_setup_InDetPerformanceMonitoringConfDbMerge = $(bin)setup_InDetPerformanceMonitoringConfDbMerge.make
cmt_dependencies_in_InDetPerformanceMonitoringConfDbMerge = $(bin)dependencies_InDetPerformanceMonitoringConfDbMerge.in
#cmt_final_setup_InDetPerformanceMonitoringConfDbMerge = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringConfDbMergesetup.make
cmt_local_InDetPerformanceMonitoringConfDbMerge_makefile = $(bin)InDetPerformanceMonitoringConfDbMerge.make

else

cmt_final_setup_InDetPerformanceMonitoringConfDbMerge = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoringConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoringConfDbMerge = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoringConfDbMerge_makefile = $(bin)InDetPerformanceMonitoringConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoringConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoringConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoringConfDbMerge/
#InDetPerformanceMonitoringConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: InDetPerformanceMonitoringConfDbMerge InDetPerformanceMonitoringConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/user/s/sthenkel/work/Performance/Alignment/WORK_ATHENA/Fresh/InnerDetector/InDetMonitoring/InDetPerformanceMonitoring/genConf/InDetPerformanceMonitoring/InDetPerformanceMonitoring.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

InDetPerformanceMonitoringConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  InDetPerformanceMonitoringConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

InDetPerformanceMonitoringConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libInDetPerformanceMonitoring_so_dependencies = ../x86_64-slc6-gcc48-opt/libInDetPerformanceMonitoring.so
#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoringConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringConfDbMergeclean ::
#-- end of cleanup_header ---------------
