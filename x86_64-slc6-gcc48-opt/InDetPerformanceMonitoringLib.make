#-- start of make_header -----------------

#====================================
#  Library InDetPerformanceMonitoringLib
#
#   Generated Wed Apr 27 05:19:00 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoringLib_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoringLib_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoringLib

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringLib = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringLib.make
cmt_local_tagfile_InDetPerformanceMonitoringLib = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringLib.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringLib = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoringLib = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoringLib)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoringLib)

ifdef cmt_InDetPerformanceMonitoringLib_has_target_tag

cmt_final_setup_InDetPerformanceMonitoringLib = $(bin)setup_InDetPerformanceMonitoringLib.make
cmt_dependencies_in_InDetPerformanceMonitoringLib = $(bin)dependencies_InDetPerformanceMonitoringLib.in
#cmt_final_setup_InDetPerformanceMonitoringLib = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringLibsetup.make
cmt_local_InDetPerformanceMonitoringLib_makefile = $(bin)InDetPerformanceMonitoringLib.make

else

cmt_final_setup_InDetPerformanceMonitoringLib = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoringLib = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoringLib = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoringLib_makefile = $(bin)InDetPerformanceMonitoringLib.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoringLib :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoringLib'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoringLib/
#InDetPerformanceMonitoringLib::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

InDetPerformanceMonitoringLiblibname   = $(bin)$(library_prefix)InDetPerformanceMonitoringLib$(library_suffix)
InDetPerformanceMonitoringLiblib       = $(InDetPerformanceMonitoringLiblibname).a
InDetPerformanceMonitoringLibstamp     = $(bin)InDetPerformanceMonitoringLib.stamp
InDetPerformanceMonitoringLibshstamp   = $(bin)InDetPerformanceMonitoringLib.shstamp

InDetPerformanceMonitoringLib :: dirs  InDetPerformanceMonitoringLibLIB
	$(echo) "InDetPerformanceMonitoringLib ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#InDetPerformanceMonitoringLibLIB :: $(InDetPerformanceMonitoringLiblib) $(InDetPerformanceMonitoringLibshstamp)
InDetPerformanceMonitoringLibLIB :: $(InDetPerformanceMonitoringLibshstamp)
	$(echo) "InDetPerformanceMonitoringLib : library ok"

$(InDetPerformanceMonitoringLiblib) :: $(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(InDetPerformanceMonitoringLiblib) $?
	$(lib_silent) $(ranlib) $(InDetPerformanceMonitoringLiblib)
	$(lib_silent) cat /dev/null >$(InDetPerformanceMonitoringLibstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(InDetPerformanceMonitoringLiblibname).$(shlibsuffix) :: $(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o $(use_requirements) $(InDetPerformanceMonitoringLibstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o $(InDetPerformanceMonitoringLib_shlibflags)
	$(lib_silent) cat /dev/null >$(InDetPerformanceMonitoringLibstamp) && \
	  cat /dev/null >$(InDetPerformanceMonitoringLibshstamp)

$(InDetPerformanceMonitoringLibshstamp) :: $(InDetPerformanceMonitoringLiblibname).$(shlibsuffix)
	$(lib_silent) if test -f $(InDetPerformanceMonitoringLiblibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(InDetPerformanceMonitoringLibstamp) && \
	  cat /dev/null >$(InDetPerformanceMonitoringLibshstamp) ; fi

InDetPerformanceMonitoringLibclean ::
	$(cleanup_echo) objects InDetPerformanceMonitoringLib
	$(cleanup_silent) /bin/rm -f $(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o) $(patsubst %.o,%.dep,$(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o) $(patsubst %.o,%.d.stamp,$(bin)IDPerfMonKshort.o $(bin)MuonSelector.o $(bin)ZmumuEvent.o $(bin)IDPerfMonZmumu.o $(bin)IDPerfMonWenu.o $(bin)IDPerfMonEoverP.o $(bin)EventAnalysis.o $(bin)TRT_Electron_Monitoring_Tool.o $(bin)IDPerfMonZee.o $(bin)PerfMonServices.o $(bin)IDPerfMuonRefitter.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf InDetPerformanceMonitoringLib_deps InDetPerformanceMonitoringLib_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
InDetPerformanceMonitoringLibinstallname = $(library_prefix)InDetPerformanceMonitoringLib$(library_suffix).$(shlibsuffix)

InDetPerformanceMonitoringLib :: InDetPerformanceMonitoringLibinstall ;

install :: InDetPerformanceMonitoringLibinstall ;

InDetPerformanceMonitoringLibinstall :: $(install_dir)/$(InDetPerformanceMonitoringLibinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(InDetPerformanceMonitoringLibinstallname) :: $(bin)$(InDetPerformanceMonitoringLibinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetPerformanceMonitoringLibinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##InDetPerformanceMonitoringLibclean :: InDetPerformanceMonitoringLibuninstall

uninstall :: InDetPerformanceMonitoringLibuninstall ;

InDetPerformanceMonitoringLibuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetPerformanceMonitoringLibinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMonKshort.d

$(bin)$(binobj)IDPerfMonKshort.d :

$(bin)$(binobj)IDPerfMonKshort.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMonKshort.o : $(src)IDPerfMonKshort.cxx
	$(cpp_echo) $(src)IDPerfMonKshort.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonKshort_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonKshort_cppflags) $(IDPerfMonKshort_cxx_cppflags)  $(src)IDPerfMonKshort.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMonKshort_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMonKshort.cxx

$(bin)$(binobj)IDPerfMonKshort.o : $(IDPerfMonKshort_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMonKshort.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonKshort_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonKshort_cppflags) $(IDPerfMonKshort_cxx_cppflags)  $(src)IDPerfMonKshort.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MuonSelector.d

$(bin)$(binobj)MuonSelector.d :

$(bin)$(binobj)MuonSelector.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)MuonSelector.o : $(src)MuonSelector.cxx
	$(cpp_echo) $(src)MuonSelector.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(MuonSelector_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(MuonSelector_cppflags) $(MuonSelector_cxx_cppflags)  $(src)MuonSelector.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(MuonSelector_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)MuonSelector.cxx

$(bin)$(binobj)MuonSelector.o : $(MuonSelector_cxx_dependencies)
	$(cpp_echo) $(src)MuonSelector.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(MuonSelector_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(MuonSelector_cppflags) $(MuonSelector_cxx_cppflags)  $(src)MuonSelector.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ZmumuEvent.d

$(bin)$(binobj)ZmumuEvent.d :

$(bin)$(binobj)ZmumuEvent.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)ZmumuEvent.o : $(src)ZmumuEvent.cxx
	$(cpp_echo) $(src)ZmumuEvent.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(ZmumuEvent_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(ZmumuEvent_cppflags) $(ZmumuEvent_cxx_cppflags)  $(src)ZmumuEvent.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(ZmumuEvent_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)ZmumuEvent.cxx

$(bin)$(binobj)ZmumuEvent.o : $(ZmumuEvent_cxx_dependencies)
	$(cpp_echo) $(src)ZmumuEvent.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(ZmumuEvent_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(ZmumuEvent_cppflags) $(ZmumuEvent_cxx_cppflags)  $(src)ZmumuEvent.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMonZmumu.d

$(bin)$(binobj)IDPerfMonZmumu.d :

$(bin)$(binobj)IDPerfMonZmumu.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMonZmumu.o : $(src)IDPerfMonZmumu.cxx
	$(cpp_echo) $(src)IDPerfMonZmumu.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonZmumu_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonZmumu_cppflags) $(IDPerfMonZmumu_cxx_cppflags)  $(src)IDPerfMonZmumu.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMonZmumu_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMonZmumu.cxx

$(bin)$(binobj)IDPerfMonZmumu.o : $(IDPerfMonZmumu_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMonZmumu.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonZmumu_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonZmumu_cppflags) $(IDPerfMonZmumu_cxx_cppflags)  $(src)IDPerfMonZmumu.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMonWenu.d

$(bin)$(binobj)IDPerfMonWenu.d :

$(bin)$(binobj)IDPerfMonWenu.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMonWenu.o : $(src)IDPerfMonWenu.cxx
	$(cpp_echo) $(src)IDPerfMonWenu.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonWenu_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonWenu_cppflags) $(IDPerfMonWenu_cxx_cppflags)  $(src)IDPerfMonWenu.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMonWenu_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMonWenu.cxx

$(bin)$(binobj)IDPerfMonWenu.o : $(IDPerfMonWenu_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMonWenu.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonWenu_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonWenu_cppflags) $(IDPerfMonWenu_cxx_cppflags)  $(src)IDPerfMonWenu.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMonEoverP.d

$(bin)$(binobj)IDPerfMonEoverP.d :

$(bin)$(binobj)IDPerfMonEoverP.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMonEoverP.o : $(src)IDPerfMonEoverP.cxx
	$(cpp_echo) $(src)IDPerfMonEoverP.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonEoverP_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonEoverP_cppflags) $(IDPerfMonEoverP_cxx_cppflags)  $(src)IDPerfMonEoverP.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMonEoverP_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMonEoverP.cxx

$(bin)$(binobj)IDPerfMonEoverP.o : $(IDPerfMonEoverP_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMonEoverP.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonEoverP_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonEoverP_cppflags) $(IDPerfMonEoverP_cxx_cppflags)  $(src)IDPerfMonEoverP.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventAnalysis.d

$(bin)$(binobj)EventAnalysis.d :

$(bin)$(binobj)EventAnalysis.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)EventAnalysis.o : $(src)EventAnalysis.cxx
	$(cpp_echo) $(src)EventAnalysis.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(EventAnalysis_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(EventAnalysis_cppflags) $(EventAnalysis_cxx_cppflags)  $(src)EventAnalysis.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(EventAnalysis_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)EventAnalysis.cxx

$(bin)$(binobj)EventAnalysis.o : $(EventAnalysis_cxx_dependencies)
	$(cpp_echo) $(src)EventAnalysis.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(EventAnalysis_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(EventAnalysis_cppflags) $(EventAnalysis_cxx_cppflags)  $(src)EventAnalysis.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TRT_Electron_Monitoring_Tool.d

$(bin)$(binobj)TRT_Electron_Monitoring_Tool.d :

$(bin)$(binobj)TRT_Electron_Monitoring_Tool.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)TRT_Electron_Monitoring_Tool.o : $(src)TRT_Electron_Monitoring_Tool.cxx
	$(cpp_echo) $(src)TRT_Electron_Monitoring_Tool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(TRT_Electron_Monitoring_Tool_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(TRT_Electron_Monitoring_Tool_cppflags) $(TRT_Electron_Monitoring_Tool_cxx_cppflags)  $(src)TRT_Electron_Monitoring_Tool.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(TRT_Electron_Monitoring_Tool_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)TRT_Electron_Monitoring_Tool.cxx

$(bin)$(binobj)TRT_Electron_Monitoring_Tool.o : $(TRT_Electron_Monitoring_Tool_cxx_dependencies)
	$(cpp_echo) $(src)TRT_Electron_Monitoring_Tool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(TRT_Electron_Monitoring_Tool_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(TRT_Electron_Monitoring_Tool_cppflags) $(TRT_Electron_Monitoring_Tool_cxx_cppflags)  $(src)TRT_Electron_Monitoring_Tool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMonZee.d

$(bin)$(binobj)IDPerfMonZee.d :

$(bin)$(binobj)IDPerfMonZee.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMonZee.o : $(src)IDPerfMonZee.cxx
	$(cpp_echo) $(src)IDPerfMonZee.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonZee_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonZee_cppflags) $(IDPerfMonZee_cxx_cppflags)  $(src)IDPerfMonZee.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMonZee_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMonZee.cxx

$(bin)$(binobj)IDPerfMonZee.o : $(IDPerfMonZee_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMonZee.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMonZee_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMonZee_cppflags) $(IDPerfMonZee_cxx_cppflags)  $(src)IDPerfMonZee.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PerfMonServices.d

$(bin)$(binobj)PerfMonServices.d :

$(bin)$(binobj)PerfMonServices.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)PerfMonServices.o : $(src)PerfMonServices.cxx
	$(cpp_echo) $(src)PerfMonServices.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(PerfMonServices_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(PerfMonServices_cppflags) $(PerfMonServices_cxx_cppflags)  $(src)PerfMonServices.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(PerfMonServices_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)PerfMonServices.cxx

$(bin)$(binobj)PerfMonServices.o : $(PerfMonServices_cxx_dependencies)
	$(cpp_echo) $(src)PerfMonServices.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(PerfMonServices_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(PerfMonServices_cppflags) $(PerfMonServices_cxx_cppflags)  $(src)PerfMonServices.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetPerformanceMonitoringLibclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)IDPerfMuonRefitter.d

$(bin)$(binobj)IDPerfMuonRefitter.d :

$(bin)$(binobj)IDPerfMuonRefitter.o : $(cmt_final_setup_InDetPerformanceMonitoringLib)

$(bin)$(binobj)IDPerfMuonRefitter.o : $(src)IDPerfMuonRefitter.cxx
	$(cpp_echo) $(src)IDPerfMuonRefitter.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMuonRefitter_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMuonRefitter_cppflags) $(IDPerfMuonRefitter_cxx_cppflags)  $(src)IDPerfMuonRefitter.cxx
endif
endif

else
$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(IDPerfMuonRefitter_cxx_dependencies)

$(bin)InDetPerformanceMonitoringLib_dependencies.make : $(src)IDPerfMuonRefitter.cxx

$(bin)$(binobj)IDPerfMuonRefitter.o : $(IDPerfMuonRefitter_cxx_dependencies)
	$(cpp_echo) $(src)IDPerfMuonRefitter.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetPerformanceMonitoringLib_pp_cppflags) $(lib_InDetPerformanceMonitoringLib_pp_cppflags) $(IDPerfMuonRefitter_pp_cppflags) $(use_cppflags) $(InDetPerformanceMonitoringLib_cppflags) $(lib_InDetPerformanceMonitoringLib_cppflags) $(IDPerfMuonRefitter_cppflags) $(IDPerfMuonRefitter_cxx_cppflags)  $(src)IDPerfMuonRefitter.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringLibclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoringLib.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringLibclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library InDetPerformanceMonitoringLib
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)InDetPerformanceMonitoringLib$(library_suffix).a $(library_prefix)InDetPerformanceMonitoringLib$(library_suffix).$(shlibsuffix) InDetPerformanceMonitoringLib.stamp InDetPerformanceMonitoringLib.shstamp
#-- end of cleanup_library ---------------
