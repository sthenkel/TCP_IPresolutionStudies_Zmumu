#-- start of make_header -----------------

#====================================
#  Document InDetPerformanceMonitoringMergeComponentsList
#
#   Generated Wed Apr 27 05:23:05 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetPerformanceMonitoringMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetPerformanceMonitoringMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetPerformanceMonitoringMergeComponentsList

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList = $(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringMergeComponentsList.make
cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList = $(bin)$(InDetPerformanceMonitoring_tag)_InDetPerformanceMonitoringMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList)
#-include $(cmt_local_tagfile_InDetPerformanceMonitoringMergeComponentsList)

ifdef cmt_InDetPerformanceMonitoringMergeComponentsList_has_target_tag

cmt_final_setup_InDetPerformanceMonitoringMergeComponentsList = $(bin)setup_InDetPerformanceMonitoringMergeComponentsList.make
cmt_dependencies_in_InDetPerformanceMonitoringMergeComponentsList = $(bin)dependencies_InDetPerformanceMonitoringMergeComponentsList.in
#cmt_final_setup_InDetPerformanceMonitoringMergeComponentsList = $(bin)InDetPerformanceMonitoring_InDetPerformanceMonitoringMergeComponentsListsetup.make
cmt_local_InDetPerformanceMonitoringMergeComponentsList_makefile = $(bin)InDetPerformanceMonitoringMergeComponentsList.make

else

cmt_final_setup_InDetPerformanceMonitoringMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetPerformanceMonitoringMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetPerformanceMonitoringMergeComponentsList = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_InDetPerformanceMonitoringMergeComponentsList_makefile = $(bin)InDetPerformanceMonitoringMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#InDetPerformanceMonitoringMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetPerformanceMonitoringMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetPerformanceMonitoringMergeComponentsList/
#InDetPerformanceMonitoringMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: InDetPerformanceMonitoringMergeComponentsList InDetPerformanceMonitoringMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/InDetPerformanceMonitoring.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

InDetPerformanceMonitoringMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  InDetPerformanceMonitoringMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

InDetPerformanceMonitoringMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libInDetPerformanceMonitoring_so_dependencies = ../x86_64-slc6-gcc48-opt/libInDetPerformanceMonitoring.so
#-- start of cleanup_header --------------

clean :: InDetPerformanceMonitoringMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetPerformanceMonitoringMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetPerformanceMonitoringMergeComponentsListclean ::
#-- end of cleanup_header ---------------
