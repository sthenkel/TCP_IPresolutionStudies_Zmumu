#-- start of make_header -----------------

#====================================
#  Document install_joboptions
#
#   Generated Wed Apr 27 05:23:07 2016  by sthenkel
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_joboptions_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_joboptions_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_joboptions

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_install_joboptions = $(InDetPerformanceMonitoring_tag)_install_joboptions.make
cmt_local_tagfile_install_joboptions = $(bin)$(InDetPerformanceMonitoring_tag)_install_joboptions.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetPerformanceMonitoring_tag = $(tag)

#cmt_local_tagfile_install_joboptions = $(InDetPerformanceMonitoring_tag).make
cmt_local_tagfile_install_joboptions = $(bin)$(InDetPerformanceMonitoring_tag).make

endif

include $(cmt_local_tagfile_install_joboptions)
#-include $(cmt_local_tagfile_install_joboptions)

ifdef cmt_install_joboptions_has_target_tag

cmt_final_setup_install_joboptions = $(bin)setup_install_joboptions.make
cmt_dependencies_in_install_joboptions = $(bin)dependencies_install_joboptions.in
#cmt_final_setup_install_joboptions = $(bin)InDetPerformanceMonitoring_install_joboptionssetup.make
cmt_local_install_joboptions_makefile = $(bin)install_joboptions.make

else

cmt_final_setup_install_joboptions = $(bin)setup.make
cmt_dependencies_in_install_joboptions = $(bin)dependencies.in
#cmt_final_setup_install_joboptions = $(bin)InDetPerformanceMonitoringsetup.make
cmt_local_install_joboptions_makefile = $(bin)install_joboptions.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetPerformanceMonitoringsetup.make

#install_joboptions :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_joboptions'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_joboptions/
#install_joboptions::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/jobOptions/InDetPerformanceMonitoring

install_joboptions :: install_joboptionsinstall ;

install :: install_joboptionsinstall ;

install_joboptionsclean :: install_joboptionsuninstall

uninstall :: install_joboptionsuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_joboptionsinstall :: ;

IDPerfMon_jobOptions_py_dependencies = ../share/IDPerfMon_jobOptions.py
TRT_Monitoring_RecExCommonAddOn_jobOptions_py_dependencies = ../share/TRT_Monitoring_RecExCommonAddOn_jobOptions.py
runJPsiMonitoring_data_py_dependencies = ../share/runJPsiMonitoring_data.py
runEoverPValidationData_py_dependencies = ../share/runEoverPValidationData.py
runJPsiMonitoring_data_grid_py_dependencies = ../share/runJPsiMonitoring_data_grid.py
runEoverPValidation_py_dependencies = ../share/runEoverPValidation.py
runJPsiMonitoring_py_dependencies = ../share/runJPsiMonitoring.py
batch_EoverP_py_dependencies = ../share/batch_EoverP.py
runEoverPValidationBatch_py_dependencies = ../share/runEoverPValidationBatch.py
runZmumuValidation_in_py_dependencies = ../share/runZmumuValidation_in.py
runZmumuValidation_data_py_dependencies = ../share/runZmumuValidation_data.py
jobOptions_useGRL_py_dependencies = ../share/jobOptions_useGRL.py
runZmumuValidation_data_OnGrid_py_dependencies = ../share/runZmumuValidation_data_OnGrid.py
TrackMonitoring_py_dependencies = ../share/TrackMonitoring.py
runKShortValidation_py_dependencies = ../share/runKShortValidation.py
runMonitoringZmumuValidation_data_py_dependencies = ../share/runMonitoringZmumuValidation_data.py
runZmumuValidation_py_dependencies = ../share/runZmumuValidation.py
runKShortValidation_grid_py_dependencies = ../share/runKShortValidation_grid.py
ElectronEoverPTracking_py_dependencies = ../share/ElectronEoverPTracking.py
runMonitoringZmumuValidation_py_dependencies = ../share/runMonitoringZmumuValidation.py
runzmumu_runallmonitoring_py_dependencies = ../share/runzmumu_runallmonitoring.py
runIDMuonMonitoring_py_dependencies = ../share/runIDMuonMonitoring.py
runzmumu_runallmonitoring_ITKtester_py_dependencies = ../share/runzmumu_runallmonitoring_ITKtester.py
runzmumu_runallmonitoring_grid_py_dependencies = ../share/runzmumu_runallmonitoring_grid.py
runzmumu_runallmonitoring_sanitycheck_py_dependencies = ../share/runzmumu_runallmonitoring_sanitycheck.py


install_joboptionsinstall :: ${install_dir}/IDPerfMon_jobOptions.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/IDPerfMon_jobOptions.py :: ../share/IDPerfMon_jobOptions.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/IDPerfMon_jobOptions.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "IDPerfMon_jobOptions.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/IDPerfMon_jobOptions.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/IDPerfMon_jobOptions.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "IDPerfMon_jobOptions.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/TRT_Monitoring_RecExCommonAddOn_jobOptions.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/TRT_Monitoring_RecExCommonAddOn_jobOptions.py :: ../share/TRT_Monitoring_RecExCommonAddOn_jobOptions.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/TRT_Monitoring_RecExCommonAddOn_jobOptions.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "TRT_Monitoring_RecExCommonAddOn_jobOptions.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/TRT_Monitoring_RecExCommonAddOn_jobOptions.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/TRT_Monitoring_RecExCommonAddOn_jobOptions.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "TRT_Monitoring_RecExCommonAddOn_jobOptions.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runJPsiMonitoring_data.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runJPsiMonitoring_data.py :: ../share/runJPsiMonitoring_data.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runJPsiMonitoring_data.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runJPsiMonitoring_data.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runJPsiMonitoring_data.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runEoverPValidationData.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runEoverPValidationData.py :: ../share/runEoverPValidationData.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidationData.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runEoverPValidationData.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runEoverPValidationData.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidationData.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runEoverPValidationData.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runJPsiMonitoring_data_grid.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runJPsiMonitoring_data_grid.py :: ../share/runJPsiMonitoring_data_grid.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring_data_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runJPsiMonitoring_data_grid.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runJPsiMonitoring_data_grid.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring_data_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runJPsiMonitoring_data_grid.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runEoverPValidation.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runEoverPValidation.py :: ../share/runEoverPValidation.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runEoverPValidation.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runEoverPValidation.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runEoverPValidation.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runJPsiMonitoring.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runJPsiMonitoring.py :: ../share/runJPsiMonitoring.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runJPsiMonitoring.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runJPsiMonitoring.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runJPsiMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runJPsiMonitoring.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/batch_EoverP.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/batch_EoverP.py :: ../share/batch_EoverP.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/batch_EoverP.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "batch_EoverP.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/batch_EoverP.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/batch_EoverP.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "batch_EoverP.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runEoverPValidationBatch.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runEoverPValidationBatch.py :: ../share/runEoverPValidationBatch.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidationBatch.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runEoverPValidationBatch.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runEoverPValidationBatch.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runEoverPValidationBatch.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runEoverPValidationBatch.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runZmumuValidation_in.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runZmumuValidation_in.py :: ../share/runZmumuValidation_in.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_in.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runZmumuValidation_in.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runZmumuValidation_in.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_in.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runZmumuValidation_in.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runZmumuValidation_data.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runZmumuValidation_data.py :: ../share/runZmumuValidation_data.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runZmumuValidation_data.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runZmumuValidation_data.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runZmumuValidation_data.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/jobOptions_useGRL.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/jobOptions_useGRL.py :: ../share/jobOptions_useGRL.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/jobOptions_useGRL.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "jobOptions_useGRL.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/jobOptions_useGRL.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/jobOptions_useGRL.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "jobOptions_useGRL.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runZmumuValidation_data_OnGrid.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runZmumuValidation_data_OnGrid.py :: ../share/runZmumuValidation_data_OnGrid.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_data_OnGrid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runZmumuValidation_data_OnGrid.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runZmumuValidation_data_OnGrid.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation_data_OnGrid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runZmumuValidation_data_OnGrid.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/TrackMonitoring.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/TrackMonitoring.py :: ../share/TrackMonitoring.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/TrackMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "TrackMonitoring.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/TrackMonitoring.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/TrackMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "TrackMonitoring.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runKShortValidation.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runKShortValidation.py :: ../share/runKShortValidation.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runKShortValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runKShortValidation.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runKShortValidation.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runKShortValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runKShortValidation.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runMonitoringZmumuValidation_data.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runMonitoringZmumuValidation_data.py :: ../share/runMonitoringZmumuValidation_data.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runMonitoringZmumuValidation_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runMonitoringZmumuValidation_data.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runMonitoringZmumuValidation_data.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runMonitoringZmumuValidation_data.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runMonitoringZmumuValidation_data.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runZmumuValidation.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runZmumuValidation.py :: ../share/runZmumuValidation.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runZmumuValidation.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runZmumuValidation.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runZmumuValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runZmumuValidation.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runKShortValidation_grid.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runKShortValidation_grid.py :: ../share/runKShortValidation_grid.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runKShortValidation_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runKShortValidation_grid.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runKShortValidation_grid.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runKShortValidation_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runKShortValidation_grid.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/ElectronEoverPTracking.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/ElectronEoverPTracking.py :: ../share/ElectronEoverPTracking.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/ElectronEoverPTracking.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "ElectronEoverPTracking.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/ElectronEoverPTracking.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/ElectronEoverPTracking.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "ElectronEoverPTracking.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runMonitoringZmumuValidation.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runMonitoringZmumuValidation.py :: ../share/runMonitoringZmumuValidation.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runMonitoringZmumuValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runMonitoringZmumuValidation.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runMonitoringZmumuValidation.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runMonitoringZmumuValidation.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runMonitoringZmumuValidation.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runzmumu_runallmonitoring.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runzmumu_runallmonitoring.py :: ../share/runzmumu_runallmonitoring.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runzmumu_runallmonitoring.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runzmumu_runallmonitoring.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runzmumu_runallmonitoring.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runIDMuonMonitoring.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runIDMuonMonitoring.py :: ../share/runIDMuonMonitoring.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runIDMuonMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runIDMuonMonitoring.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runIDMuonMonitoring.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runIDMuonMonitoring.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runIDMuonMonitoring.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runzmumu_runallmonitoring_ITKtester.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runzmumu_runallmonitoring_ITKtester.py :: ../share/runzmumu_runallmonitoring_ITKtester.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_ITKtester.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runzmumu_runallmonitoring_ITKtester.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runzmumu_runallmonitoring_ITKtester.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_ITKtester.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runzmumu_runallmonitoring_ITKtester.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runzmumu_runallmonitoring_grid.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runzmumu_runallmonitoring_grid.py :: ../share/runzmumu_runallmonitoring_grid.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runzmumu_runallmonitoring_grid.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runzmumu_runallmonitoring_grid.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_grid.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runzmumu_runallmonitoring_grid.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_joboptionsinstall :: ${install_dir}/runzmumu_runallmonitoring_sanitycheck.py
	@if test ! "${installarea}" = ""; then\
	  echo "installation done"; \
	fi

${install_dir}/runzmumu_runallmonitoring_sanitycheck.py :: ../share/runzmumu_runallmonitoring_sanitycheck.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_sanitycheck.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "runzmumu_runallmonitoring_sanitycheck.py" "$(install_dir)" "ln -sf" "$($(package)_cmtpath)"; \
	fi

../share/runzmumu_runallmonitoring_sanitycheck.py : ;

install_joboptionsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../share/runzmumu_runallmonitoring_sanitycheck.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "runzmumu_runallmonitoring_sanitycheck.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi
#-- start of cleanup_header --------------

clean :: install_joboptionsclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_joboptions.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_joboptionsclean ::
#-- end of cleanup_header ---------------
